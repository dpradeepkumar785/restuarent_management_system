from django.contrib import admin
from .models import Registration,staff,Table,Reservation,category,item,Menu,cart,summary,orderTransaction,Deliveryagent,delivery


# Register your models here.
admin.site.register(Registration)
admin.site.register(staff)
admin.site.register(Table)
admin.site.register(Reservation)
admin.site.register(category)
admin.site.register(item)
admin.site.register(Menu)
admin.site.register(cart)
admin.site.register(summary)
admin.site.register(orderTransaction)
admin.site.register(Deliveryagent)
admin.site.register(delivery)