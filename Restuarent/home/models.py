from django.db import models


# Create your models here.

class Registration(models.Model):
    # Id = models.IntegerField()
    name = models.CharField(max_length=100)
    email =models.EmailField(max_length=100)
    password=models.CharField(max_length=100)
    phone_number =models.IntegerField()
    cust_Address = models.CharField(max_length=10000,default='Please contact the customer for address')
    time =models.DateTimeField(auto_now_add =True)
    
    def __str__(self):
        return f"{self.Name}"

# class customer(models.Model):
#     Id = models.IntegerField()
#     Name = models.CharField(max_length=500)
#     Address = models.CharField(max_length=10000)
#     def __str__(self):
#         return f"{self.Name}"

class staff(models.Model):
    user_name = models.CharField(max_length=500)
    Password = models.CharField(max_length=500)


class Table(models.Model):
    table_number = models.CharField(max_length=500)
    table_availability = models.BooleanField()
    # Table_size = models.IntegerField()
    def __str__(self):
        return f"Table :{self.Table_number}"

class Reservation(models.Model):
    table_number = models.ForeignKey(Table, on_delete=models.CASCADE)
    # Id = models.ForeignKey(Registration, on_delete=models.CASCADE)
    time = models.TimeField(auto_now=True)
    date = models.DateField(auto_now=True)
    number_of_persons = models.IntegerField()

class category(models.Model):
    name = models.CharField(max_length=100)
    def __str__(self):
        return f"{self.Name}"

class item(models.Model):
    item_id = models.IntegerField()
    item_name = models.CharField(max_length=500)
    price = models.IntegerField()
    category = models.ForeignKey(category,on_delete=models.CASCADE)
    
    def __str__(self):
        return f'{self.item_name}'

# class Menu(models.Model):
#     item = models.ForeignKey(item,on_delete=models.CASCADE)

class cart(models.Model):
    id = models.ForeignKey(Registration,on_delete=models.CASCADE)
    item = models.ForeignKey(item,on_delete=models.CASCADE)
    quantity = models.IntegerField()
    sub_total = models.IntegerField()
    def taxes(self):
        return f'{self.sub_total * (0.18/100)}'
    def delivery_charges(self):
        return f'{30}'

# class summary(models.Model):
#     cart = models.ForeignKey(cart,on_delete=models.CASCADE)
    

class order(models.Model):
    order_id = models.IntegerField()
    # summary = models.ForeignKey(summary,on_delete=models.CASCADE)
    amount = models.IntegerField()
    payment_status = models.CharField(max_length=500)

class orderitems(models.Model):
    order_id = models.ForeignKey(order,on_delete=models.CASCADE)
    item_name = models.ForeignKey(item,on_delete=models.CASCADE)
    quantity = models.ForeignKey(item,on_delete=models.CASCADE)

class Deliveryagent(models.Model):
    delivery_agent = models.CharField(max_length=500)
    Status = models.CharField(max_length=100)

class delivery(models.Model):
    delivery_agent = models.ForeignKey(Deliveryagent,on_delete=models.CASCADE)
    id = models.ForeignKey(Registration,on_delete=models.CASCADE)
    # Summary = models.ForeignKey(summary,on_delete=models.CASCADE)
    delivery_time = models.TimeField(default=30)



class Restaurant(models.Model):
    name = models.CharField(max_length=255)
    # other fields for the restaurant model

class TableReservation(models.Model):
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    date = models.DateField()
    time = models.TimeField()
    party_size = models.PositiveSmallIntegerField()
    customer_name = models.CharField(max_length=255)
    customer_phone = models.CharField(max_length=255)
    customer_email = models.EmailField()
    reservation_status = models.BooleanField(default=False)