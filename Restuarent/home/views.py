from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User 
from django.contrib import messages
from django.contrib.auth import authenticate ,login
from .models import *
from .models import Restaurant, TableReservation
# Create your views here.
def home(request):
    return render(request ,'index.html')
def Login_page(request):
    context={}
    if request.method =="POST":
        username =request.POST["email"]
        password1 =request.POST["password"]
        user = authenticate(username =username , password = password1)
        if user is not None:
            login(request ,user)
            return render(request ,'index.html')
        else:
            messages.error(request ,"Bad credentials")
            return redirect("home")



    return render(request,'Login.html')
def register_page(request):
    if request.method =="POST":
        #username=request.POST["username"]
        fname =request.POST["first_name"]
        lname = request.POST["last_name"]
        email =request.POST["email"]
        password =request.POST["password"]
        cnf_password =request.POST["confirm_password"]

        myuser =User.objects.create_user(email ,password)
        myuser.save()

        messages.success(request ,"Your account has been created ")
        return redirect("Login")
        context={}
    return render(request,'Register.html')

def Logout(request):
    return render(request,'index.html')

def reservation(request):
    tables = Table.objects.all()
    context = {'tables':tables}
    return render(request,'table.html')

# def booktable(request):
#     pass
# def menu(request):
#     items = item.objects.all()
#     context = {'items':items}
#     return render(request,'menu.html')
# def cartdetails(request,itemid):
#     i = item.objects.get(pk = itemid)
#     try: 
#         cartitem = cart.objects.get(item = i)
#     except cart.DoesNotExist:
#         cartitem = cart.objects.create(item = i,quantity = 0)
#     cartitem.quantity += 1
#     cartitem.save()
#     return redirect('index.html')
def booktable(request):
    pass
def menu(request):
    items = item.objects.all()
    context = {'items':items}
    return render(request,'menu.html')
def cartdetails(request,itemid):
    i = item.objects.get(pk = itemid)
    try: 
        cartitem = cart.objects.get(item = i)
    except cart.DoesNotExist:
        cartitem = cart.objects.create(item = i,quantity = 0)
    cartitem.quantity += 1
    cartitem.save()
    return redirect('index.html')
 


def make_reservation(request, restaurant_id):
    restaurant = get_object_or_404(Restaurant, id=restaurant_id)
    if request.method == 'POST':
        date = request.POST.get('date')
        time = request.POST.get('time')
        party_size = request.POST.get('party_size')
        customer_name = request.POST.get('customer_name')
        customer_phone = request.POST.get('customer_phone')
        customer_email = request.POST.get('customer_email')
        reservation = TableReservation.objects.create(restaurant=restaurant, date=date, time=time, party_size=party_size, customer_name=customer_name, customer_phone=customer_phone, customer_email=customer_email, reservation_status=True)
        return redirect('reservation_confirmation', reservation.id)
    return render(request, 'make_reservation.html', {'restaurant': restaurant})

