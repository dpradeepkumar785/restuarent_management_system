from django.urls import path ,include
from home import views


urlpatterns = [
    path('', views.home, name='home'),
    path('Login/', views.Login_page, name='Login'),
    path('Register/', views.register_page, name='register'),
    path('Logout/', views.Logout, name='Logout'),
    path('make_a_reservation/', views.reservation, name='reservation'),
    path('booktable/<int:tableno>', views.booktable, name='booktable'),
    path('menu/', views.menu, name='menu'), 
    path('cart/', views.cartdetails, name='cartdetails'),
    path('Deliveryagent/',views.Deliveryagent, name='Deliveryagent') 
]